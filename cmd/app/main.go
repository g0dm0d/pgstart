package main

import (
	"log"

	"github.com/g0dm0d/pgstart/internal/config"
	"github.com/g0dm0d/pgstart/internal/server"
	"github.com/g0dm0d/pgstart/internal/service"
	"github.com/g0dm0d/pgstart/internal/store/postgres"
)

func main() {
	config, err := config.New()
	if err != nil {
		log.Fatal(err)
	}

	db, err := postgres.New(config.Postgres.DSN)
	if err != nil {
		log.Fatal(err)
	}

	commandStore := postgres.NewCommandStore(db)
	commandLogsStore := postgres.NewCommandLogsStore(db)

	services := service.New(commandStore, commandLogsStore)

	server := server.NewServer(&server.Config{
		Addr:    config.App.Addr,
		Port:    config.App.Port,
		Service: services,
	})

	server.SetupRouter()

	log.Print("Server is up and running.")

	err = server.RunServer()
	if err != nil {
		log.Panic(err)
	}
}
