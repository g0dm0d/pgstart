package server

import (
	"fmt"
	"net/http"

	"github.com/g0dm0d/pgstart/internal/server/middleware"
	"github.com/g0dm0d/pgstart/internal/server/req"
	"github.com/g0dm0d/pgstart/internal/service"
)

type Server struct {
	server *http.Server
	router *http.ServeMux

	service *service.Service
}

type Config struct {
	Addr    string
	Port    string
	Service *service.Service
}

func NewServer(config *Config) *Server {
	return &Server{
		server: &http.Server{
			Addr:    fmt.Sprint(config.Addr, ":", config.Port),
			Handler: http.NotFoundHandler(),
		},
		router: http.NewServeMux(),

		service: config.Service,
	}
}

func (s *Server) SetupRouter() {
	s.router.Handle("GET /commands", middleware.CORS(req.NewHandler(s.service.Command.GetCommands)))
	s.router.Handle("POST /command", middleware.CORS(req.NewHandler(s.service.Command.SaveCommand)))
	s.router.Handle("GET /command/{id}", middleware.CORS(req.NewHandler(s.service.Command.GetCommand)))
	s.router.Handle("GET /command/run/{id}", middleware.CORS(req.NewHandler(s.service.Command.RunCommand)))
	s.router.Handle("GET /process/kill/{id}", middleware.CORS(req.NewHandler(s.service.Command.KillProcess)))
	s.router.Handle("GET /process/{id}", middleware.CORS(req.NewHandler(s.service.Command.GetProccess)))

	s.server.Handler = s.router
}

func (s *Server) RunServer() error {
	return s.server.ListenAndServe()
}
