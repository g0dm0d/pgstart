package command

import (
	"os/exec"

	"github.com/g0dm0d/pgstart/internal/server/req"
	"github.com/g0dm0d/pgstart/internal/store"
)

type Command interface {
	GetCommand(ctx *req.Ctx) error
	GetCommands(ctx *req.Ctx) error
	SaveCommand(ctx *req.Ctx) error
	RunCommand(ctx *req.Ctx) error
	KillProcess(ctx *req.Ctx) error
	GetProccess(ctx *req.Ctx) (err error)
}

type Service struct {
	commandStore     store.CommandStore
	commandLogsStore store.CommandLogsStore
	activeProcess    map[int]*exec.Cmd
}

func New(commandStore store.CommandStore, commandLogsStore store.CommandLogsStore) *Service {
	return &Service{
		commandStore:     commandStore,
		commandLogsStore: commandLogsStore,
		activeProcess:    make(map[int]*exec.Cmd),
	}
}
