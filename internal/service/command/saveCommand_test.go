package command_test

import (
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/g0dm0d/pgstart/internal/server/req"
	"github.com/g0dm0d/pgstart/internal/service/command"
	"github.com/g0dm0d/pgstart/internal/store/postgres"
	"github.com/stretchr/testify/require"
)

func TestSaveCommand(t *testing.T) {
	body := strings.NewReader(`{"name": "hello","command": "#!/bin/sh\necho \"Hello world!\""}`)

	r := httptest.NewRequest(http.MethodPost, "/command", body)
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	rightResp := `{
		"id": 1,
		"name": "hello",
		"command": "#!/bin/sh\necho \"Hello world!\""
	}`

	commandStore := postgres.NewCommandStore(db)
	commandLogsStore := postgres.NewCommandLogsStore(db)

	command := command.New(commandStore, commandLogsStore)
	command.SaveCommand(&req.Ctx{
		Writer:  w,
		Request: r,
	})

	res := w.Result()
	defer res.Body.Close()
	data, err := io.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}

	require.JSONEq(t, string(rightResp), string(data))

	r = httptest.NewRequest(http.MethodGet, "/command/{id}", nil)
	r.SetPathValue("id", "1")

	w = httptest.NewRecorder()

	command.GetCommand(&req.Ctx{
		Writer:  w,
		Request: r,
	})

	res = w.Result()
	defer res.Body.Close()
	data, err = io.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}

	require.JSONEq(t, string(rightResp), string(data))

	_, err = db.Exec("DELETE FROM commands; ALTER SEQUENCE commands_id_seq RESTART WITH 1")
	if err != nil {
		t.Error(err)
	}
}
