package command_test

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"testing"

	"github.com/golang-migrate/migrate/v4"
	pg "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"

	"github.com/ory/dockertest"
	"github.com/ory/dockertest/docker"
)

var db *sql.DB

func env(key, fallbackValue string) string {
	s, ok := os.LookupEnv(key)
	if !ok {
		return fallbackValue
	}
	return s
}

func TestMain(m *testing.M) {
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Fatalf("Could not construct pool: %s", err)
	}

	err = pool.Client.Ping()
	if err != nil {
		log.Fatalf("Could not connect to Docker: %s", err)
	}

	resourcePG, err := pool.RunWithOptions(&dockertest.RunOptions{
		Repository: "postgres",
		Tag:        "16.1-alpine3.18",
		Env: []string{
			"POSTGRES_PASSWORD=12345",
			"POSTGRES_USER=postgres",
			"POSTGRES_DB=postgres",
			"listen_addresses = '*'",
		},
	}, func(config *docker.HostConfig) {
		config.AutoRemove = true
		config.RestartPolicy = docker.RestartPolicy{Name: "no"}
	})
	if err != nil {
		log.Fatalf("Could not start resource: %s", err)
	}

	resourcePG.Expire(120)

	portPG := resourcePG.GetPort("5432/tcp")
	databaseUrl := fmt.Sprintf("postgres://postgres:12345@%s/postgres?sslmode=disable", fmt.Sprintf("%s:%s", env("YOUR_APP_DB_HOST", "localhost"), portPG))

	log.Println("Connecting to database on url: ", databaseUrl)

	if err = pool.Retry(func() error {
		db, err = sql.Open("postgres", databaseUrl)
		if err != nil {
			return err
		}
		return db.Ping()
	}); err != nil {
		log.Fatalf("Could not connect to docker: %s", err)
	}

	driver, err := pg.WithInstance(db, &pg.Config{})
	gom, err := migrate.NewWithDatabaseInstance("file://./../../store/migrations/", "postgres", driver)
	if err != nil {
		log.Fatal(err)
	}

	err = gom.Up()
	if err != nil {
		log.Fatal(err)
	}

	code := m.Run()

	if err := pool.Purge(resourcePG); err != nil {
		log.Fatalf("Could not purge resource: %s", err)
	}

	os.Exit(code)
}
