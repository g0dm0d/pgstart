package command_test

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/g0dm0d/pgstart/internal/server/req"
	"github.com/g0dm0d/pgstart/internal/service/command"
	"github.com/g0dm0d/pgstart/internal/store/postgres"
	"github.com/stretchr/testify/require"
)

func TestGetNotExistCommand(t *testing.T) {
	r := httptest.NewRequest(http.MethodGet, "/command/{id}", nil)
	r.SetPathValue("id", "1")

	w := httptest.NewRecorder()

	rightResp := `{
		"code": 104,
		"description": "Command not found",
		"Status": 400
	}`

	commandStore := postgres.NewCommandStore(db)
	commandLogsStore := postgres.NewCommandLogsStore(db)

	command := command.New(commandStore, commandLogsStore)
	command.GetCommand(&req.Ctx{
		Writer:  w,
		Request: r,
	})

	res := w.Result()
	defer res.Body.Close()
	data, err := io.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}

	require.JSONEq(t, string(rightResp), string(data))
}
