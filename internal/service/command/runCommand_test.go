package command_test

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/g0dm0d/pgstart/internal/server/req"
	"github.com/g0dm0d/pgstart/internal/service/command"
	"github.com/g0dm0d/pgstart/internal/store/postgres"
)

func TestRunCommand(t *testing.T) {
	body := strings.NewReader(`{"name": "hello","command": "#!/bin/sh\necho \"Hello world!\""}`)

	r := httptest.NewRequest(http.MethodPost, "/command", body)
	r.Header.Add("Content-Type", "application/json")

	w := httptest.NewRecorder()

	commandStore := postgres.NewCommandStore(db)
	commandLogsStore := postgres.NewCommandLogsStore(db)

	command := command.New(commandStore, commandLogsStore)
	command.SaveCommand(&req.Ctx{
		Writer:  w,
		Request: r,
	})

	r = httptest.NewRequest(http.MethodGet, "/command/run/{id}", nil)
	r.SetPathValue("id", "1")

	w = httptest.NewRecorder()

	command.GetCommand(&req.Ctx{
		Writer:  w,
		Request: r,
	})

	res := w.Result()
	if res.StatusCode != 200 {
		t.Errorf("expected status code 200 got %v", res.StatusCode)
	}

	_, err := db.Exec("DELETE FROM commands; ALTER SEQUENCE commands_id_seq RESTART WITH 1")
	if err != nil {
		t.Error(err)
	}
}
