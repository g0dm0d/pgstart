package command

import (
	"github.com/g0dm0d/pgstart/internal/server/req"
	"github.com/g0dm0d/pgstart/pkg/errs"
)

func (s *Service) GetCommands(ctx *req.Ctx) (err error) {
	commands, err := s.commandStore.GetCommands(ctx.Context())
	if err != nil {
		return errs.ReturnError(ctx.Writer, errs.CommandNotFound)
	}

	return ctx.JSON(commands)
}
