package command

import (
	"bufio"
	"context"
	"log"
	"os/exec"
	"strconv"

	"github.com/g0dm0d/pgstart/internal/server/req"
	"github.com/g0dm0d/pgstart/pkg/errs"
)

const (
	StatusError      = 3
	StatusAwait      = 2
	StatusInProgress = 1
	StatusDone       = 0
)

type RunCommandResp struct {
	ProcessID int `json:"process_id"`
}

func (s *Service) RunCommand(ctx *req.Ctx) (err error) {
	idString := ctx.Request.PathValue("id")

	id, err := strconv.Atoi(idString)
	if err != nil {
		return errs.ReturnError(ctx.Writer, errs.InvalidURLParam)
	}

	command, err := s.commandStore.GetCommand(ctx.Context(), id)
	if err != nil {
		return errs.ReturnError(ctx.Writer, errs.CommandNotFound)
	}

	logID, err := s.commandLogsStore.CreateLogs(ctx.Context(), id, StatusAwait)
	if err != nil {
		return err
	}

	cmd := exec.CommandContext(context.Background(), "sh", "-c", command.Command)

	cmdReader, _ := cmd.StdoutPipe()
	scanner := bufio.NewScanner(cmdReader)
	out := make(chan string)
	go reader(scanner, out)

	err = cmd.Start()
	if err != nil {
		return s.commandLogsStore.UpdateStatus(context.Background(), logID, StatusError)
	}
	err = s.commandLogsStore.UpdateStatus(context.Background(), logID, StatusInProgress)
	if err != nil {
		return err
	}

	s.activeProcess[logID] = cmd

	done := make(chan error)
	go func() {
		done <- cmd.Wait()
	}()

	go func() {
		for {
			select {
			case text := <-out:
				err := s.commandLogsStore.UpdateLogs(context.Background(), logID, text)
				if err != nil {
					log.Println(err)
				}
			case <-done:
				err = s.commandLogsStore.UpdateStatus(context.Background(), logID, StatusDone)
				if err != nil {
					log.Println(err)
				}
				delete(s.activeProcess, logID)
			}
		}
	}()

	return ctx.JSON(RunCommandResp{ProcessID: logID})
}

func reader(scanner *bufio.Scanner, out chan string) {
	for scanner.Scan() {
		out <- scanner.Text() + "\n"
	}
}
