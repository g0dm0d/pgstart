package command

import (
	"strconv"

	"github.com/g0dm0d/pgstart/internal/server/req"
	"github.com/g0dm0d/pgstart/pkg/errs"
)

func (s *Service) GetProccess(ctx *req.Ctx) (err error) {
	idString := ctx.Request.PathValue("id")

	id, err := strconv.Atoi(idString)
	if err != nil {
		return errs.ReturnError(ctx.Writer, errs.InvalidURLParam)
	}

	command, err := s.commandLogsStore.GetLogs(ctx.Context(), id)
	if err != nil {
		return errs.ReturnError(ctx.Writer, errs.ProcessNotFound)
	}

	return ctx.JSON(command)
}
