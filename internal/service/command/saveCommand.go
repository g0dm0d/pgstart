package command

import (
	"github.com/g0dm0d/pgstart/internal/server/req"
	"github.com/g0dm0d/pgstart/pkg/errs"
)

type SaveCommandReq struct {
	Name    string `json:"name"`
	Command string `json:"command"`
}

func (s *Service) SaveCommand(ctx *req.Ctx) error {
	var request SaveCommandReq

	err := ctx.ParseJSON(&request)
	if err != nil {
		return errs.ReturnError(ctx.Writer, errs.InvalidJSON)
	}

	command, err := s.commandStore.SaveCommand(ctx.Context(), request.Name, request.Command)
	if err != nil {
		return errs.ReturnError(ctx.Writer, errs.InternalServerError)
	}

	return ctx.JSON(command)
}
