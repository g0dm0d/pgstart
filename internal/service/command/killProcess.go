package command

import (
	"strconv"

	"github.com/g0dm0d/pgstart/internal/server/req"
	"github.com/g0dm0d/pgstart/pkg/errs"
)

func (s *Service) KillProcess(ctx *req.Ctx) (err error) {
	idString := ctx.Request.PathValue("id")

	id, err := strconv.Atoi(idString)
	if err != nil {
		return errs.ReturnError(ctx.Writer, errs.InvalidURLParam)
	}

	cmd, ok := s.activeProcess[id]
	if !ok {
		return errs.ReturnError(ctx.Writer, errs.ProcessNotFound)
	}

	return cmd.Cancel()
}
