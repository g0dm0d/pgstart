package command

import (
	"strconv"

	"github.com/g0dm0d/pgstart/internal/server/req"
	"github.com/g0dm0d/pgstart/pkg/errs"
)

func (s *Service) GetCommand(ctx *req.Ctx) (err error) {
	idString := ctx.Request.PathValue("id")

	id, err := strconv.Atoi(idString)
	if err != nil {
		return errs.ReturnError(ctx.Writer, errs.InvalidURLParam)
	}

	command, err := s.commandStore.GetCommand(ctx.Context(), id)
	if err != nil {
		return errs.ReturnError(ctx.Writer, errs.CommandNotFound)
	}

	return ctx.JSON(command)
}
