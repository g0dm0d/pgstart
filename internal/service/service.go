package service

import (
	"github.com/g0dm0d/pgstart/internal/service/command"
	"github.com/g0dm0d/pgstart/internal/store"
)

type Service struct {
	Command command.Command
}

func New(commandStore store.CommandStore, commandLogsStore store.CommandLogsStore) *Service {
	return &Service{
		Command: command.New(commandStore, commandLogsStore),
	}
}
