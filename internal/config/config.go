package config

import (
	"os"
)

type App struct {
	Addr string
	Port string
}

type Postgres struct {
	DSN string
}

type Nats struct {
	ClusterID string
	ClientID  string
	URL       string
}

type Config struct {
	App      App
	Postgres Postgres
	Nats     Nats
}

func New() (*Config, error) {
	return &Config{
		App: App{
			Addr: getEnv("APP_ADDR"),
			Port: getEnv("APP_PORT"),
		},
		Postgres: Postgres{
			DSN: getEnv("POSTGRES_DSN"),
		},
	}, nil
}

func getEnv(key string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}
	return ""
}
