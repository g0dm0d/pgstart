CREATE TABLE IF NOT EXISTS commands (
  id      SERIAL      PRIMARY KEY,
  name    VARCHAR(40) NOT NULL,
  command TEXT        NOT NULL
);

CREATE TABLE IF NOT EXISTS execution_logs (
  id          SERIAL      PRIMARY KEY,
  logs        TEXT        DEFAULT NULL,
  status      SMALLINT    NOT NULL,
  command_id  INT         REFERENCES commands(id)
);