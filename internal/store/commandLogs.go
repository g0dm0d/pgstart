package store

import (
	"context"
	"database/sql"

	"github.com/g0dm0d/pgstart/internal/model"
)

type CommandLogs struct {
	ID        int
	Logs      sql.NullString
	Status    int
	CommandID int
}

type CommandLogsStore interface {
	CreateLogs(ctx context.Context, commandID int, status int) (int, error)
	UpdateLogs(ctx context.Context, id int, logs string) error
	UpdateStatus(ctx context.Context, id int, status int) error
	GetLogs(ctx context.Context, processID int) (model.CommandLogs, error)
}
