package postgres

import (
	"context"
	"database/sql"

	"github.com/g0dm0d/pgstart/internal/model"
	"github.com/g0dm0d/pgstart/internal/store"
)

type CommandLogsStore struct {
	db *sql.DB
}

func NewCommandLogsStore(db *sql.DB) store.CommandLogsStore {
	return &CommandLogsStore{
		db: db,
	}
}

func (s *CommandLogsStore) CreateLogs(ctx context.Context, commandID int, status int) (int, error) {
	query := `INSERT INTO execution_logs (command_id, status) 
	VALUES ($1, $2)
	RETURNING id`

	var id int
	err := s.db.QueryRowContext(ctx, query, commandID, status).Scan(
		&id,
	)

	if err != nil {
		return 0, err
	}

	return id, nil
}

func (s *CommandLogsStore) UpdateLogs(ctx context.Context, id int, newLogs string) error {
	query := `SELECT logs FROM execution_logs WHERE id = $1`
	tx, err := s.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}

	var logs sql.NullString
	err = tx.QueryRowContext(ctx, query, id).Scan(&logs)
	if err != nil {
		return err
	}

	query = `UPDATE execution_logs SET logs = $1 WHERE id = $2`

	_, err = tx.ExecContext(ctx, query, logs.String+newLogs, id)
	if err != nil {
		return err
	}

	return tx.Commit()
}

func (s *CommandLogsStore) UpdateStatus(ctx context.Context, id int, status int) error {
	query := `UPDATE execution_logs SET status = $1 WHERE id = $2`

	_, err := s.db.ExecContext(ctx, query, status, id)
	if err != nil {
		return err
	}

	return nil
}

func (s *CommandLogsStore) GetLogs(ctx context.Context, processID int) (model.CommandLogs, error) {
	query := `SELECT id, logs, status, command_id FROM execution_logs WHERE id = $1`

	var logs model.CommandLogs
	err := s.db.QueryRowContext(ctx, query, processID).Scan(
		&logs.ID,
		&logs.Logs,
		&logs.Status,
		&logs.CommandID,
	)

	if err != nil {
		return model.CommandLogs{}, err
	}

	return logs, nil
}
