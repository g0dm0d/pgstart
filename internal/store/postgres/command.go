package postgres

import (
	"context"
	"database/sql"

	"github.com/g0dm0d/pgstart/internal/model"
	"github.com/g0dm0d/pgstart/internal/store"
)

type CommandStore struct {
	db *sql.DB
}

func NewCommandStore(db *sql.DB) store.CommandStore {
	return &CommandStore{
		db: db,
	}
}

func (s *CommandStore) SaveCommand(ctx context.Context, commandName, command string) (model.Command, error) {
	query := `INSERT INTO commands (
		name,
		command
	)
	VALUES ($1, $2)
	RETURNING id, name, command`

	var commandModel model.Command
	err := s.db.QueryRowContext(ctx, query, commandName, command).Scan(
		&commandModel.ID,
		&commandModel.Name,
		&commandModel.Command,
	)
	if err != nil {
		return model.Command{}, err
	}

	return commandModel, nil
}

func (s *CommandStore) GetCommand(ctx context.Context, id int) (model.Command, error) {
	query := `SELECT id, name, command FROM commands
	WHERE id = $1`

	var commandModel model.Command
	err := s.db.QueryRowContext(ctx, query, id).Scan(
		&commandModel.ID,
		&commandModel.Name,
		&commandModel.Command,
	)
	if err != nil {
		return model.Command{}, err
	}

	return commandModel, nil
}

func (s *CommandStore) GetCommands(ctx context.Context) ([]model.Command, error) {
	query := `SELECT id, name, command FROM commands`

	var commands []model.Command
	rows, err := s.db.QueryContext(ctx, query)
	if err != nil {
		return []model.Command{}, err
	}

	for rows.Next() {
		var command model.Command
		rows.Scan(&command.ID, &command.Name, &command.Command)
		commands = append(commands, command)
	}

	return commands, nil
}
