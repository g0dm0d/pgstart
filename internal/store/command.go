package store

import (
	"context"

	"github.com/g0dm0d/pgstart/internal/model"
)

type Command struct {
	ID      int
	Name    string
	Command string
}

type CommandStore interface {
	SaveCommand(ctx context.Context, commandName, command string) (model.Command, error)
	GetCommand(ctx context.Context, id int) (model.Command, error)
	GetCommands(ctx context.Context) ([]model.Command, error)
}
