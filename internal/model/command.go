package model

type Command struct {
	ID      int    `json:"id"`
	Command string `json:"command"`
	Name    string `json:"name"`
}
