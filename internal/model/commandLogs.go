package model

type CommandLogs struct {
	ID        int
	Logs      string
	Status    int
	CommandID int
}
