# Installation

### Docker compose

```shell
make setup
make docker_compose_run
```

### Local develop

```shell
make setup

# setup .env file
vim .env

make migration_up
make run
```

# Documentation

[OpenAPI yaml doc](https://gitlab.com/g0dm0d/pgstart/-/blob/main/api/openapi.yaml?ref_type=heads)