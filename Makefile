include .env

.PHONY: install run migration_up migration_down docker_build docker_compose_run docker_clean

migration_up:
	migrate -path ./internal/store/migrations/ -database "$(POSTGRES_DSN)" -verbose up

migration_down:
	migrate -path ./internal/store/migrations/ -database "$(POSTGRES_DSN)" -verbose down

setup:
	cp ./configs/.env.example .env
	go mod tody

docker_build:
	docker build -f ./deployments/Dockerfile -t pgstart:dev .

docker_compose_run:
	docker compose up

docker_clean:
	docker stop pgstart
	docker rm pgstart
	docker rmi pgstart:dev

run:
	go run cmd/app/main.go