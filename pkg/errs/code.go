package errs

import "net/http"

type Code struct {
	Code        uint16 `json:"code"`
	Description string `json:"description"`
	Status      int
}

var (
	InternalServerError = Code{Code: 100, Description: "Internal server error", Status: http.StatusInternalServerError}
	InvalidID           = Code{Code: 101, Description: "Invalid ID", Status: http.StatusBadRequest}
	InvalidURLParam     = Code{Code: 102, Description: "Invalid url param", Status: http.StatusBadRequest}
	InvalidJSON         = Code{Code: 103, Description: "Invalid json body", Status: http.StatusBadRequest}
	CommandNotFound     = Code{Code: 104, Description: "Command not found", Status: http.StatusBadRequest}
	ProcessNotFound     = Code{Code: 105, Description: "Process not found", Status: http.StatusBadRequest}
)
